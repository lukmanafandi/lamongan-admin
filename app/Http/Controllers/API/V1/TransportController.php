<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Transport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TransportController extends Controller
{
    public function index()
    {
        $transports = Transport::latest()->get();
        $transports->map(function ($transport) {
            $transport->photo = env('APP_URL') . Storage::url('transports/' . $transport->photo);
            return $transport;
        });
        return response()->json([
            'success' => true,
            'message' => 'List Data Transport',
            'data'    => $transports
        ], 200);
    }

    public function show($id)
    {
        $transport = Transport::find($id);

        $transport->photo = env('APP_URL') . Storage::url('transports/' . $transport->photo);

        if (!$transport) {
            return response()->json([
                'success' => false,
                'message' => 'Transport tidak ditemukan',
                'data'    => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Detail Data Transport',
            'data'    => $transport
        ], 200);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'price' => 'required|max:255',
            'rating' => 'required|max:255',
            'is_recommended' => 'required|max:255|default:0',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'type' => 'required|max:255',
            'number_phone' => 'required|max:255',
        ]);

        $transport = new Transport();

        $transport->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/transports', $photo, $filename);
            $transport->photo = $filename;
        }

        $transport->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Transport berhasil disimpan',
            'data'    => $transport
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $transport = Transport::find($id);

        if (!$transport) {
            return response()->json([
                'success' => false,
                'message' => 'Data Transport dengan id ' . $id . ' tidak ditemukan',
                'data'    => null
            ], 404);
        }

        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'price' => 'required|max:255',
            'rating' => 'required|max:255',
            'is_recommended' => 'required|max:255|default:0',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'type' => 'required|max:255',
            'number_phone' => 'required|max:255',
        ]);

        $transport->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/transports', $photo, $filename);
            Storage::delete('public/transports/' . $transport->photo);
            $transport->photo = $filename;
        }

        $transport->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Transport berhasil diupdate',
            'data'    => $transport
        ], 200);
    }

    public function destroy($id)
    {
        $transport = Transport::find($id);

        if (!$transport) {
            return response()->json([
                'success' => false,
                'message' => 'Data Transport dengan id ' . $id . ' tidak ditemukan',
                'data'    => null
            ], 404);
        }

        Storage::delete('public/transports/' . $transport->photo);

        $transport->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Transport berhasil dihapus',
            'data'    => $transport
        ], 200);
    }
}
