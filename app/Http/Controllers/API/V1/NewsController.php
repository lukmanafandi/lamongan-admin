<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    // 'title',
    // 'content',
    // 'photo',
    // 'author',
    // 'category'

    public function index()
    {
        $news = News::latest()->get();
        $news->map(function ($news) {
            $news->photo = env('APP_URL') . Storage::url('news/' . $news->photo);
            return $news;
        });
        return response()->json([
            'success' => true,
            'message' => 'List Data News',
            'data'    => $news
        ], 200);
    }

    public function show($id)
    {
        $news = News::find($id);

        $news->photo = env('APP_URL') . Storage::url('news/' . $news->photo);

        if (!$news) {
            return response()->json([
                'success' => false,
                'message' => 'News tidak ditemukan',
                'data'    => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Detail Data News',
            'data'    => $news
        ], 200);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author' => 'required|max:255',
            'category' => 'required|max:255',
        ]);

        $news = new News();

        $news->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/news/photos', $photo, $fileName);

            $news->photo = 'photos/' . $fileName;
        }

        $news->save();

        return response()->json([
            'success' => true,
            'message' => 'Berhasil menambahkan data news',
            'data'    => $news
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $news = News::find($id);

        if (!$news) {
            return response()->json([
                'success' => false,
                'message' => 'News tidak ditemukan',
                'data'    => null
            ], 404);
        }

        $validateData = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author' => 'required|max:255',
            'category' => 'required|max:255',
        ]);

        $news->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/news/photos', $photo, $fileName);
            Storage::delete('public/news/photos/' . $news->photo);

            $news->photo = 'photos/' . $fileName;
        }

        $news->save();

        return response()->json([
            'success' => true,
            'message' => 'Berhasil mengubah data news',
            'data'    => $news
        ], 200);
    }

    public function destroy($id)
    {
        $news = News::find($id);

        if (!$news) {
            return response()->json([
                'success' => false,
                'message' => 'News tidak ditemukan',
                'data'    => null
            ], 404);
        }

        $news->delete();

        return response()->json([
            'success' => true,
            'message' => 'Berhasil menghapus data news',
            'data'    => $news
        ], 200);
    }
}
