<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Campus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CampusController extends Controller
{
    public function index()
    {
        $campuses = Campus::latest()->get();
        $campuses->map(function ($campus) {
            $campus->photo = env('APP_URL') . Storage::url('campuses/' . $campus->photo);
            return $campus;
        });
        return response()->json([
            'success' => true,
            'message' => 'List Data Campus',
            'data'    => $campuses
        ], 200);
    }

    public function show($id)
    {
        $campus = Campus::find($id);

        $campus->photo = env('APP_URL') . Storage::url('campuses/' . $campus->photo);

        if (!$campus) {
            return response()->json([
                'success' => false,
                'message' => 'Campus tidak ditemukan',
                'data'    => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Detail Data Campus',
            'data'    => $campus
        ], 200);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'email' => 'required|max:255',
            'number_phone' => 'required|max:255',
            'website' => 'required|max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $campus = new Campus();

        $campus->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/campuses', $photo, $filename);
            $campus->photo = $filename;
        }

        $campus->save();

        return response()->json([
            'success' => true,
            'message' => 'Campus berhasil ditambahkan',
            'data'    => $campus
        ], 201);
    }

    public function update(Request $request, $id)
    {
        $campus =  Campus::find($id);

        if (!$campus) {
            return response()->json([
                'success' => false,
                'message' => 'Campus tidak ditemukan',
                'data'    => null
            ], 404);
        }

        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'email' => 'required|max:255',
            'number_phone' => 'required|max:255',
            'website' => 'required|max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $campus->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/campuses', $photo, $filename);
            Storage::delete('public/campuses/' . $campus->photo);
            $campus->photo = $filename;
        }

        $campus->save();

        return response()->json([
            'success' => true,
            'message' => 'Campus berhasil diupdate',
            'data'    => $campus
        ], 200);
    }

    public function destroy($id)
    {
        $campus = Campus::find($id);

        if (!$campus) {
            return response()->json([
                'success' => false,
                'message' => 'Campus tidak ditemukan',
                'data'    => null
            ], 404);
        }

        Storage::delete('public/campuses/' . $campus->photo);
        $campus->delete();

        return response()->json([
            'success' => true,
            'message' => 'Campus berhasil dihapus',
            'data'    => $campus
        ], 200);
    }
}
