<?php

namespace App\Http\Controllers;

use App\Models\Transport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TransportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $transports = Transport::latest();

        if ($request->has('search') && $request->search != '') {
            $transports = $transports->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('description', 'like', '%' . $request->search . '%')
                ->orWhere('address', 'like', '%' . $request->search . '%')
                ->orWhere('price', 'like', '%' . $request->search . '%');
        }

        $transports = $transports->paginate(10);

        return view('transports.index',  ['transports' => $transports, 'type_menu' => 'transport']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transports.create', ['type_menu' => 'transport']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'address' => 'required|max:255',
            'price' => 'required|max:255',
            'time' => 'required|max:255',
            'capacity' => 'required|max:255',
            'city' => 'required|max:255',
            'owner' => 'required|max:255',
            'type' => 'required|max:255',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'number_phone' => 'required|max:255',
        ]);

        $transport = new Transport();
        $transport->fill($validateData);
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/transports/photos', $photo, $fileName);

            $transport->photo = 'photos/' . $fileName;
        }

        $transport->save();

        return redirect()->route('transports.index')->with('success', 'Transport successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transport  $transport
     * @return \Illuminate\Http\Response
     */
    public function show(Transport $transport)
    {
        return view('transports.show', ['transport' => $transport, 'type_menu' => 'transport']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transport  $transport
     * @return \Illuminate\Http\Response
     */
    public function edit(Transport $transport)
    {
        return view('transports.edit', ['transport' => $transport, 'type_menu' => 'transport']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transport  $transport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transport $transport)
    {
        $validateData = $request->validate([
            'name' => 'max:255',
            'description' => 'max:255',
            'address' => 'max:255',
            'price' => 'max:255',
            'time' => 'max:255',
            'capacity' => 'max:255',
            'city' => 'max:255',
            'owner' => 'max:255',
            'type' => 'max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'number_phone' => 'max:255',
        ]);

        $transport->fill($validateData);
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/transports/photos', $photo, $fileName);

            $transport->photo = 'photos/' . $fileName;
        }

        $transport->save();

        return redirect()->route('transports.index')->with('success', 'Transport successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transport  $transport
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transport $transport)
    {
        $transport->delete();

        return redirect()->route('transports.index')->with('success', 'Transport successfully deleted');
    }
}
