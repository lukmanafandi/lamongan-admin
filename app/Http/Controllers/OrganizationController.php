<?php

namespace App\Http\Controllers;

use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $organizations = Organization::latest();

        if ($request->has('search') && $request->search != '') {
            $organizations = $organizations->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('email', 'like', '%' . $request->search . '%')
                ->orWhere('number_phone', 'like', '%' . $request->search . '%')
                ->orWhere('address', 'like', '%' . $request->search . '%')
                ->orWhere('instagram', 'like', '%' . $request->search . '%')
                ->orWhere('description', 'like', '%' . $request->search . '%');
        }

        $organizations = $organizations->paginate(10);

        return view('organizations.index', ['organizations' => $organizations, 'type_menu' => 'organizations']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organizations.create', ['type_menu' => 'organizations']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'number_phone' => 'required|max:255',
            'address' => 'required|max:255',
            'instagram' => 'required|max:255',
            'description' => 'required|max:255',
        ]);

        $organization = new Organization();

        $organization->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/organizations/photos', $photo, $fileName);

            $organization->photo = 'photos/' . $fileName;
        }

        $organization->save();

        return redirect()->route('organizations.index')->with('success', 'Organization created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        return view('organizations.show', compact('organization'), ['type_menu' => 'organizations']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        return view('organizations.edit', compact('organization'), ['type_menu' => 'organizations']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {

        $validateData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'number_phone' => 'required|max:255',
            'address' => 'required|max:255',
            'instagram' => 'required|max:255',
            'description' => 'required|max:255',
        ]);

        $organization->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/organizations/photos', $photo, $fileName);

            $organization->photo = 'photos/' . $fileName;
        }

        $organization->save();

        return redirect()->route('organizations.index')->with('success', 'Organization updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {

        $organization->delete();

        return redirect()->route('organizations.index')->with('success', 'Organization deleted successfully.');
    }
}
