<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $news = News::latest();

        if ($request->has('search') && $request->search != '') {
            $news = $news->where('title', 'like', '%' . $request->search . '%')
                ->orWhere('content', 'like', '%' . $request->search . '%')
                ->orWhere('author', 'like', '%' . $request->search . '%')
                ->orWhere('category', 'like', '%' . $request->search . '%');
        }

        $news = $news->paginate(10);

        return view('news.index',  ['news' => $news, 'type_menu' => 'news']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create', ['type_menu' => 'news']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author' => 'required|max:255',
            'category' => 'required|max:255',
        ]);

        $news = new News();

        $news->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/news/photos', $photo, $fileName);

            $news->photo = 'photos/' . $fileName;
        }

        $news->save();

        return redirect()->route('news.index')->with('success', 'News created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {

        return view('news.show', compact('news'), ['type_menu' => 'news']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {

        return view('news.edit', compact('news'), ['type_menu' => 'news']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {

        $validateData = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'author' => 'required|max:255',
            'category' => 'required|max:255',
        ]);

        $news->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/news/photos', $photo, $fileName);

            $news->photo = 'photos/' . $fileName;
        }

        $news->save();

        return redirect()->route('news.index')->with('success', 'News updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return redirect()->route('news.index')->with('success', 'News deleted successfully.');
    }
}
