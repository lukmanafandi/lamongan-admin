<?php

namespace App\Http\Controllers;

use App\Models\Culinary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CulinaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $culinaries = Culinary::latest();

        if ($request->has('search') && $request->search != '') {
            $culinaries = $culinaries->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('description', 'like', '%' . $request->search . '%')
                ->orWhere('address', 'like', '%' . $request->search . '%')
                ->orWhere('price', 'like', '%' . $request->search . '%')
                ->orWhere('rating', 'like', '%' . $request->search . '%')
                ->orWhere('is_recommended', 'like', '%' . $request->search . '%');
        }

        $culinaries = $culinaries->paginate(10);

        return view('culinaries.index',  ['culinaries' => $culinaries, 'type_menu' => 'culinaries']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('culinaries.create', ['type_menu' => 'culinaries']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'address' => 'required|max:255',
            'price' => 'required|max:255',
            'rating' => 'required|max:255',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'type' => 'required|max:255',
            'number_phone' => 'required|max:255',
        ]);

        $culinary = new Culinary();

        $culinary->fill($validateData);
        $culinary->type = 'Food';
        $culinary->is_recommended = $request->has('is_recommended');
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/culinaries/photos', $photo, $fileName);
            $culinary->photo = 'photos/' . $fileName;
        }

        $culinary->save();

        return redirect()->route('culinaries.index')->with('success', 'Culinary successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Culinary  $culinary
     * @return \Illuminate\Http\Response
     */
    public function show(Culinary $culinary)
    {
        return view('culinaries.show', ['culinary' => $culinary, 'type_menu' => 'culinaries']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Culinary  $culinary
     * @return \Illuminate\Http\Response
     */
    public function edit(Culinary $culinary)
    {
        return view('culinaries.edit', ['culinary' => $culinary, 'type_menu' => 'culinaries']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Culinary  $culinary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Culinary $culinary)
    {
        $validateData = $request->validate([
            'name' => 'max:255',
            'description' => '',
            'address' => 'max:255',
            'price' => 'max:255',
            'rating' => 'max:255',
            'number_phone' => 'max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'type' => 'max:255'
        ]);

        $culinary->fill($validateData);
        $culinary->is_recommended = false;
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/culinaries/photos', $photo, $fileName);

            $culinary->photo = 'photos/' . $fileName;
        }

        $culinary->save();

        return redirect()->route('culinaries.index')->with('success', 'Culinary successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Culinary  $culinary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Culinary $culinary)
    {
        $culinary->delete();

        return redirect()->route('culinaries.index')->with('success', 'Culinary successfully deleted');
    }
}
