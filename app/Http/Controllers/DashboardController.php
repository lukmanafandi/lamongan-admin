<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Campus;
use App\Models\Culinary;
use App\Models\Organization;
use App\Models\Transport;
use App\Models\News;
use App\Models\Dorm;

class DashboardController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
			$data['campuses_count'] = Campus::count();
			$data['culinary_count'] = Culinary::count();
			$data['organization_count'] = Organization::count();
			$data['transport_count'] = Transport::count();
			$data['news_count'] = News::count();
			$data['dorm_count'] = Dorm::count();

      return view('dashboard', ['data' => $data]);
    }
}
