@extends('layouts.app')

@section('name', 'Create Culinary')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Culinary</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Culinary</a></div>
                    <div class="breadcrumb-item"><a href="#">Create</a></div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <form action="{{ route('culinaries.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-header">
                                    <h4>Create Culinary</h4>
                                </div>
                                <div class="card-body row">
                                    <div class="col-md-6 col-lg-6">
                                        {{-- form name --}}
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror "
                                                name="name" value="{{ old('name') }}" required="">
                                            @error('name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form address --}}
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text"
                                                class="form-control @error('address') is-invalid @enderror " name="address"
                                                value="{{ old('address') }}" required="">
                                            @error('address')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form number_phone --}}
                                        <div class="form-group">
                                            <label>Number Phone</label>
                                            <input type="text"
                                                class="form-control @error('number_phone') is-invalid @enderror "
                                                name="number_phone" value="{{ old('number_phone') }}" required="">
                                            @error('number_phone')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form is_recommended --}}
                                        <div class="form-group">
                                            <div class="control-label">Is Recommended</div>
                                            <label class="custom-switch mt-2">
                                                <input type="checkbox" value="true" name="is_recommended"
                                                    class="custom-switch-input">
                                                <span class="custom-switch-indicator"></span>
                                            </label>
                                            @error('is_recommended')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">

                                        {{-- form description --}}
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea type="text" class="form-control @error('description') is-invalid @enderror " name="description"
                                                value="" required="" style="height: 100px;">{{ old('description') }}</textarea>
                                            @error('description')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form photo --}}
                                        <div class="form-group">
                                            <label>Photo</label>
                                            <input type="file" class="form-control @error('photo') is-invalid @enderror "
                                                name="photo" value="@old('photo'))">
                                            @error('photo')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form price --}}
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input class="form-control @error('price') is-invalid @enderror " name="price"
                                                value="{{ @old('price') }}" required="" type="text">
                                            @error('price')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        {{-- form rating --}}
                                        <div class="form-group">
                                            <label>Rating</label>

                                        </div>
                                        <div class="form-group">
                                            <div class="selectgroup selectgroup-pills">
                                                <label class="selectgroup-item">
                                                    <input type="radio" name="rating" value="1"
                                                        class="selectgroup-input" checked="">
                                                    <span class="selectgroup-button selectgroup-button-icon"><i
                                                            class="fas fa-star"></i></span>
                                                </label>
                                                <label class="selectgroup-item">
                                                    <input type="radio" name="rating" value="2"
                                                        class="selectgroup-input">
                                                    <span class="selectgroup-button selectgroup-button-icon"><i
                                                            class="fas fa-star"></i></span>
                                                </label>
                                                <label class="selectgroup-item">
                                                    <input type="radio" name="rating" value="3"
                                                        class="selectgroup-input">
                                                    <span class="selectgroup-button selectgroup-button-icon"><i
                                                            class="fas fa-star"></i></span>
                                                </label>
                                                <label class="selectgroup-item">
                                                    <input type="radio" name="rating" value="4"
                                                        class="selectgroup-input">
                                                    <span class="selectgroup-button selectgroup-button-icon"><i
                                                            class="fas fa-star"></i></span>
                                                </label>
                                                <label class="selectgroup-item">
                                                    <input type="radio" name="rating" value="5"
                                                        class="selectgroup-input">
                                                    <span class="selectgroup-button selectgroup-button-icon"><i
                                                            class="fas fa-star"></i></span>
                                                </label>
                                            </div>
                                            @error('rating')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form type --}}
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control selectric @error('type') is-invalid @enderror"
                                                name="type" required="">
                                                <option value="food">Food</option>
                                                <option value="drink">Drink</option>
                                            </select>
                                            @error('type')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
