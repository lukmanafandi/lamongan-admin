@extends('layouts.app')

@section('name', 'Create Transport')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Transport</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Transport</a></div>
                    <div class="breadcrumb-item"><a href="#">Create</a></div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <form action="{{ route('transports.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-header">
                                    <h4>Create Transport</h4>
                                </div>
                                <div class="card-body row">
                                    <div class="col-md-6 col-lg-6">
                                        {{-- form name --}}
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror "
                                                name="name" value="{{ old('name') }}" required="">
                                            @error('name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form address --}}
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text"
                                                class="form-control @error('address') is-invalid @enderror " name="address"
                                                value="{{ old('address') }}" required="">
                                            @error('address')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form number_phone --}}
                                        <div class="form-group">
                                            <label>Number Phone</label>
                                            <input type="text"
                                                class="form-control @error('number_phone') is-invalid @enderror "
                                                name="number_phone" value="{{ old('number_phone') }}" required="">
                                            @error('number_phone')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-md-6 col-lg-6">

                                        {{-- form description --}}
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea type="text" class="form-control @error('description') is-invalid @enderror " name="description"
                                                value="" required="" style="height: 100px;">{{ old('description') }}</textarea>
                                            @error('description')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form photo --}}
                                        <div class="form-group">
                                            <label>Photo</label>
                                            <input type="file" class="form-control @error('photo') is-invalid @enderror "
                                                name="photo" value="@old('photo'))">
                                            @error('photo')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form price --}}
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input class="form-control @error('price') is-invalid @enderror " name="price"
                                                value="{{ @old('price') }}" required="" type="text">
                                            @error('price')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form time --}}
                                        <div class="form-group">
                                            <label>Time</label>
                                            <select class="form-control selectric @error('time') is-invalid @enderror"
                                                name="time" required="">
                                                <option value="day">Day</option>
                                                <option value="week">Week</option>
                                                <option value="month">Month</option>
                                                <option value="year">Year</option>
                                            </select>
                                            @error('time')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form capacity --}}
                                        <div class="form-group">
                                            <label>Capacity</label>
                                            <input type="text"
                                                class="form-control @error('capacity') is-invalid @enderror "
                                                name="capacity" value="{{ old('capacity') }}" required="">
                                            @error('capacity')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form city --}}
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control @error('city') is-invalid @enderror "
                                                name="city" value="{{ old('city') }}" required="">
                                            @error('city')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form owner --}}
                                        <div class="form-group">
                                            <label>Owner</label>
                                            <input type="text" class="form-control @error('owner') is-invalid @enderror "
                                                name="owner" value="{{ old('owner') }}" required="">
                                            @error('owner')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>


                                        {{-- form type --}}
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control selectric @error('type') is-invalid @enderror"
                                                name="type" required="">
                                                <option value="car">Car</option>
                                                <option value="motorcycle">Motorcycle</option>
                                                <option value="bicycle">Bicycle</option>
                                                <option value="mixed">Mixed</option>
                                            </select>
                                            @error('type')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
