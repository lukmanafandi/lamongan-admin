@extends('layouts.app')

@section('title', 'Edit Dorm')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Dorm</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dorm</a></div>
                    <div class="breadcrumb-item"><a href="#">Edit</a></div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <form action="{{ route('dorms.update', ['dorm' => $dorm]) }}" method="POST"
                                enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="card-header">
                                    <h4>Edit Dorm</h4>
                                </div>
                                <div class="card-body row">
                                    <div class="col-md-6 col-lg-6">
                                        {{-- form name --}}
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror "
                                                name="name" value="{{ $dorm->name }}" required="">
                                            @error('name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form address --}}
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text"
                                                class="form-control @error('address') is-invalid @enderror " name="address"
                                                value="{{ $dorm->address }}" required="">
                                            @error('address')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form number_phone --}}
                                        <div class="form-group">
                                            <label>Number Phone</label>
                                            <input type="text"
                                                class="form-control @error('number_phone') is-invalid @enderror "
                                                name="number_phone" value="{{ $dorm->number_phone }}" required="">
                                            @error('number_phone')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">

                                        {{-- form description --}}
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control @error('description') is-invalid @enderror " name="description" value=""
                                                required="" style="height: 100px;">{{ $dorm->description }}t</textarea>
                                            @error('description')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form photo cover --}}
                                        <div class="form-group">
                                            <label>Photo</label>
                                            @if ($dorm->photo)
                                                <figure class="imagecheck-figure">
                                                    <img src="{{ asset('storage/dorms/' . $dorm->photo) }}" alt=""
                                                        class="imagecheck-image">
                                                </figure>
                                            @endif

                                            <input type="file" class="form-control @error('photo') is-invalid @enderror "
                                                name="photo" value="@$dorm->photo)">
                                            @error('photo')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror

                                        </div>

                                        {{-- form price --}}
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input class="form-control @error('price') is-invalid @enderror " name="price"
                                                value="{{ $dorm->price }}" required="" type="text">
                                            @error('price')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form time --}}
                                        <div class="form-group">
                                            <label>Time</label>
                                            <select class="form-control selectric @error('time') is-invalid @enderror"
                                                name="time" required="" value={{ $dorm->time }}>
                                                <option value="day">Day</option>
                                                <option value="week">Week</option>
                                                <option value="month">Month</option>
                                                <option value="year">Year</option>
                                            </select>
                                            @error('time')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form facilities --}}
                                        <div class="form-group">
                                            <label>Facilities</label>
                                            <input type="text"
                                                class="form-control @error('facilities') is-invalid @enderror "
                                                name="facilities" value="{{ $dorm->facilities }}" required="">
                                            @error('facilities')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form city --}}
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control @error('city') is-invalid @enderror "
                                                name="city" value="{{ $dorm->city }}" required="">
                                            @error('city')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form owner --}}
                                        <div class="form-group">
                                            <label>Owner</label>
                                            <input type="text" class="form-control @error('owner') is-invalid @enderror "
                                                name="owner" value="{{ $dorm->owner }}" required="">
                                            @error('owner')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>



                                        {{-- form type --}}
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control selectric @error('type') is-invalid @enderror"
                                                value="{{ $dorm->type }}" name="type" id="type">

                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="mixed">Mixed</option>
                                            </select>
                                            </select>
                                            @error('type')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
